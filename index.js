const express = require("express");
const socket = require("socket.io");
const Utlis = require('./utlis');
const redisAdapter = require('socket.io-redis');
// App setup
const PORT = 8900;
const app = express();
const server = app.listen(PORT, function () {
    console.log(`Listening on port ${PORT}`);
});

const io = socket(server);
const redis = Utlis.redisConnection()
//utils.redisQuery = utils.redisConnection();

io.adapter(redisAdapter({
    pubClient: Utlis.redisConnection(),
    subClient: Utlis.redisConnection()
}));

redis.psubscribe(
    'NOTIFICATIONS*',
    function (err, count) { });

redis.on("pmessage", function (wc, channel, data) {
    if (channel === 'NOTIFICATIONS:EVENT'){
        io.emit('event-message', data);
    }
    else if (channel === `NOTIFICATIONS:${data._id}:QUOTATION:VENDOR`){
        io.emit('vendor-quotation', data);
    }   
    else if (channel === `NOTIFICATIONS:QUOTATION:ADMIN`){
        io.emit('admin-quotation', data);
    }  
    else if (channel === `NOTIFICATIONS:${data._id}:KAIZEN:VENDOR`){
        io.emit('vendor-kazien', data);
    }  
    else if (channel === `NOTIFICATIONS:KAIZEN:ADMIN`){
        io.emit('admin-kaizen', data);
    }  
});

io.on("connection", function (socket) {
    socket.emit('ping', 'pong')
});

