require('dotenv').config()
const Redis = require('ioredis');
class Utlis  {
    redisConnection() {
       
        return new Redis({
            port: process.env.REDIS_PORT,
            host: process.env.REDIS_HOST
        });
    }
}
module.exports = new Utlis